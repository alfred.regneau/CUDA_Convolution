#ifndef CONVOLUTION_H
#define CONVOLUTION_H

#include <vector>

using namespace std;


// Calcul la somme total des valeurs d'un kernel
float sum_kernel(vector<float> kernel) {
  float sum = 0;
  for (int i=0; i<kernel.size(); i++) {
    sum += kernel[i];
  }
  return sum;
}

// Pour les kernels ayant une somme supérieur à 1
vector<float> normalize_kernel(vector<float> kernel) {
  float sum = sum_kernel(kernel);
  if (sum > 1.0) {
    for (int i=0; i<kernel.size(); i++) {
      kernel[i] /= sum;
    }
  }
  return kernel;
}

// Listes des kernels disponible pour la convolution

vector<float> BOX_BLUR_KERNEL = {
  1.0/9.0, 1.0/9.0, 1.0/9.0,
  1.0/9.0, 1.0/9.0, 1.0/9.0,
  1.0/9.0, 1.0/9.0, 1.0/9.0
};

vector<float> GAUSSIAN_BLUR_KERNEL = {
  0.0,  0.0,  0.0,  5.0,  0.0,  0.0,  0.0,
  0.0,  5.0, 18.0, 32.0, 18.0,  5.0,  0.0,
  0.0, 18.0, 64.0,100.0, 64.0, 18.0,  0.0,
  5.0, 32.0,100.0,100.0,100.0, 32.0,  5.0,
  0.0, 18.0, 64.0,100.0, 64.0, 18.0,  0.0,
  0.0,  5.0, 18.0, 32.0, 18.0,  5.0,  0.0,
  0.0,  0.0,  0.0,  5.0,  0.0,  0.0,  0.0,
};

vector<float> HORIZONTAL_LINES_DETECTION_KERNEL = {
  -1.0, -1.0, -1.0,
   2.0,  2.0,  2.0,
  -1.0, -1.0, -1.0
};
vector<float> VERTICAL_LINES_DETECTION_KERNEL = {
  -1.0,  2.0, -1.0,
  -1.0,  2.0, -1.0,
  -1.0,  2.0, -1.0
};
vector<float> LINES_45DEGREES_DETECTION_KERNEL = {
  -1.0, -1.0,  2.0,
  -1.0,  2.0, -1.0,
   2.0, -1.0, -1.0
};
vector<float> LINES_135DEGREES_DETECTION_KERNEL = {
   2.0, -1.0, -1.0,
  -1.0,  2.0, -1.0,
  -1.0, -1.0,  2.0
};
vector<float> EDGE_DETECTION_KERNEL = {
  -1.0, -1.0, -1.0,
  -1.0,  8.0, -1.0,
  -1.0, -1.0, -1.0
};


vector<float> HORIZONTAL_SOBEL_EDGE_OPERATOR_KERNEL = {
  -1.0, -2.0, -1.0,
   0.0,  0.0,  0.0,
   1.0,  2.0,  1.0
};
vector<float> VERTICAL_SOBEL_EDGE_OPERATOR_KERNEL = {
  -1.0,  0.0,  1.0,
  -2.0,  0.0,  2.0,
  -1.0,  0.0,  1.0
};


vector<float> LAPLACIAN_OPERATOR_KERNEL = {
   0.0, -1.0,  0.0,
  -1.0,  4.0, -1.0,
   0.0, -1.0,  0.0
};
vector<float> LAPLACIAN_DIAGONAL_OPERATOR_KERNEL = {
  -1.0, -1.0, -1.0,
  -1.0,  8.0, -1.0,
  -1.0, -1.0, -1.0
};

vector<float> LAPLACIAN_GAUSSIAN_KERNEL = {
   0.0,  0.0, -1.0,  0.0,  0.0,
   0.0, -1.0, -2.0, -1.0,  0.0,
  -1.0, -2.0, 16.0, -2.0, -1.0,
   0.0, -1.0, -2.0, -1.0,  0.0,
   0.0,  0.0, -1.0,  0.0,  0.0,
};

#endif
