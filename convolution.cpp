#include <opencv2/opencv.hpp>
#include <vector>
#include <chrono>
#include <sys/stat.h>

#include "convolution.hpp"

using namespace std;

vector<unsigned char> convolution(cv::Mat m_in, vector<float> kernel) {
    auto rgb = m_in.data;
    vector<unsigned char> g(m_in.rows * m_in.cols * 3);

    int kernel_size = sqrt(kernel.size());
    int offset = kernel_size/2;

    for (int x=offset; x<m_in.cols-offset; x++) {
      for (int y=offset; y<m_in.rows-offset; y++) {
        vector<int> new_rgb = {0, 0, 0};
        for (int i=0; i<kernel_size; i++) {
          for (int j=0; j<kernel_size; j++) {
              int xn = x + i - offset;
              int yn = y + j - offset;
              int index = 3*(xn+m_in.cols*yn);
              new_rgb[0] += rgb[index+0] * kernel[j*kernel_size+i];
              new_rgb[1] += rgb[index+1] * kernel[j*kernel_size+i];
              new_rgb[2] += rgb[index+2] * kernel[j*kernel_size+i];
          }
        }
        for (int i = 0; i < 3; i++) {
          if (new_rgb[i] > 255)
            new_rgb[i] = 255;
          if (new_rgb[i] < 0)
            new_rgb[i] = 0;
        }
        int index = 3*(x+m_in.cols*y);
        // g[index] = gray;
        g[index+0] = new_rgb[0];
        g[index+1] = new_rgb[1];
        g[index+2] = new_rgb[2];
      }
    }
    return g;
}


int main()
{
  // Sélection des images à tester
  vector<string> images = {
    "cubes480x270.jpg",
    "cubes960x540.jpg",
    "cubes1920x1080.jpg",
    "cubes3840x2160.jpg",
    "cubes7680x4320.jpg",
    "cubes15360x8640.jpg",
  };
  // Sélection des kernels à tester
  map<string, vector<float>> kernels = {
    {"Simple_box_blur", BOX_BLUR_KERNEL},
    {"Gaussian_blur", GAUSSIAN_BLUR_KERNEL},
    {"Horizontal_lines_detection", HORIZONTAL_LINES_DETECTION_KERNEL},
    {"Laplacian_of_Gaussian", LAPLACIAN_GAUSSIAN_KERNEL},
  };

  cout << "--------------------- VERSION CPU ---------------------" << endl;

  for (auto kernelEntry : kernels) {

    cout << "Kernel : " << kernelEntry.first << endl;

    vector<float> kernel = normalize_kernel(kernelEntry.second);

    for (string image : images) {

        cv::Mat m_in = cv::imread("images/" + image, cv::IMREAD_UNCHANGED );

        auto start = chrono::system_clock::now();

        vector<unsigned char> g = convolution(m_in, kernel);

        auto stop = chrono::system_clock::now();

        cout << "\tImage : " << image << ", time : ";
        cout << chrono::duration_cast<chrono::milliseconds>(stop -start).count() << "ms" << endl;

        cv::Mat m_out( m_in.rows, m_in.cols, CV_8UC3, g.data() );

        string out_directory = "out/cpu/" + kernelEntry.first + "/";
        if (!system(("mkdir -p " + out_directory).c_str()))
          cv::imwrite( out_directory + image, m_out );
    }
  }

  return 0;
}
