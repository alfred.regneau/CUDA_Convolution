#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <sys/stat.h>

#include "convolution.hpp"

using namespace std;

__global__ void convolution(unsigned char * rgb, unsigned char * s,
                            std::size_t cols, std::size_t rows,
                            float * kernel, int kernel_size) {

    auto x = blockIdx.x * blockDim.x + threadIdx.x;
    auto y = blockIdx.y * blockDim.y + threadIdx.y;

    int offset = kernel_size/2;
    auto red = 0;
    auto green = 0;
    auto blue = 0;
    if( x > offset && x < cols-offset && y > offset && y < rows-offset )
    {
      for (int i=0; i<kernel_size; i++) {
        for (int j=0; j<kernel_size; j++) {
            int xn = x + i - offset;
            int yn = y + j - offset;
            int index = 3*(xn+cols*yn);
            red += rgb[index+0] * kernel[j*kernel_size+i];
            green += rgb[index+1] * kernel[j*kernel_size+i];
            blue += rgb[index+2] * kernel[j*kernel_size+i];
        }
      }

      if (red > 255) red = 255;
      if (red < 0) red = 0;
      if (green > 255) green = 255;
      if (green < 0) green = 0;
      if (blue > 255) blue = 255;
      if (blue < 0) blue = 0;

      int index = 3*(x+cols*y);

      s[index+0] = red;
      s[index+1] = green;
      s[index+2] = blue;
    }
}

int main()
{
  // Sélection des images à tester
  vector<string> images = {
    "cubes480x270.jpg",
    "cubes960x540.jpg",
    "cubes1920x1080.jpg",
    "cubes3840x2160.jpg",
    "cubes7680x4320.jpg",
    "cubes15360x8640.jpg",
  };
  // Sélection des kernel à tester
  map<string, vector<float>> kernels = {
    {"Simple_box_blur", BOX_BLUR_KERNEL},
    {"Gaussian_blur", GAUSSIAN_BLUR_KERNEL},
    {"Horizontal_lines_detection", HORIZONTAL_LINES_DETECTION_KERNEL},
    {"Laplacian_of_Gaussian", LAPLACIAN_GAUSSIAN_KERNEL},
  };


  cout << "--------------------- VERSION GPU ---------------------" << endl;

  for (auto kernelEntry : kernels) {

    cout << "Kernel : " << kernelEntry.first << endl;

    vector<float> kernel = normalize_kernel(kernelEntry.second);

    for (string image : images) {

      cv::Mat m_in = cv::imread("images/" + image, cv::IMREAD_UNCHANGED );

      cudaError_t cudaStatus;
      cudaError_t kernelStatus;

      //auto rgb = m_in.data;
      auto rows = m_in.rows;
      auto cols = m_in.cols;

      //std::vector< unsigned char > g( rows * cols );
      // Allocation de l'image de sortie en RAM côté CPU.
      unsigned char * g = nullptr;
      cudaMallocHost( &g, 3 * rows * cols );
      cv::Mat m_out( rows, cols, CV_8UC3, g );

      // Copie de l'image en entrée dans une mémoire dite "pinned" de manière à accélérer les transferts.
      // OpenCV alloue la mémoire en interne lors de la décompression de l'image donc soit sans doute avec
      // un malloc standard.
      unsigned char * rgb = nullptr;
      cudaStatus = cudaMallocHost( &rgb, 3 * rows * cols );
      if (cudaStatus != cudaSuccess)
    	   std::cout << "Error CudaMalloc rgb"  << " ";

      std::memcpy( rgb, m_in.data, 3 * rows * cols * sizeof( unsigned char) );

      unsigned char * rgb_d = nullptr;
      unsigned char * s_d = nullptr;

      float * kernel_d = nullptr;

      cudaStatus = cudaMalloc( &rgb_d, 3 * rows * cols );
      if (cudaStatus != cudaSuccess)
    	   std::cout << "Error CudaMalloc rgb_d"  << " ";
      cudaStatus = cudaMalloc( &s_d, 3 * rows * cols );
      if (cudaStatus != cudaSuccess)
    	   std::cout << "Error CudaMalloc s_d"  << " ";
      cudaStatus = cudaMalloc( &kernel_d, kernel.size() * sizeof(float));
      if (cudaStatus != cudaSuccess)
    	   std::cout << "Error CudaMalloc kernel_d"  << " ";

      cudaStatus = cudaMemcpy( rgb_d, rgb, 3 * rows * cols, cudaMemcpyHostToDevice );
      if (cudaStatus != cudaSuccess)
         std::cout << "Error CudaMemcpy rgb_d - HostToDevice"  << " ";
      cudaStatus = cudaMemcpy( kernel_d, kernel.data(), kernel.size() * sizeof(float), cudaMemcpyHostToDevice );
      if (cudaStatus != cudaSuccess)
    	   std::cout << "Error CudaMemcpy kernel_d - HostToDevice"  << " ";

      dim3 block( 32, 32 );
      dim3 grid0( ( cols - 1) / block.x + 1 , ( rows - 1 ) / block.y + 1 );

      cudaEvent_t start, stop;

      cudaEventCreate( &start );
      cudaEventCreate( &stop );

      // Mesure du temps de calcul du kernel uniquement.
      cudaEventRecord( start );

      convolution<<< grid0, block >>>( rgb_d, s_d, cols, rows, kernel_d, sqrt(kernel.size()) );

      cudaEventRecord( stop );

      kernelStatus = cudaGetLastError();
      if ( kernelStatus != cudaSuccess )
    	   std::cout << "CUDA Error"<< cudaGetErrorString(kernelStatus) << " ";

      cudaStatus = cudaMemcpy( g, s_d, 3 * rows * cols, cudaMemcpyDeviceToHost );
      if (cudaStatus != cudaSuccess)
    	   std::cout << "Error CudaMemcpy s_d - DeviceToHost"  << " ";

      cudaEventSynchronize( stop );
      float duration;
      cudaEventElapsedTime( &duration, start, stop );
      std::cout << "\tImage : " << image << ", time : ";
      std::cout << duration << "ms" << std::endl;
      cudaEventDestroy(start);
      cudaEventDestroy(stop);

      string out_directory = "out/gpu/" + kernelEntry.first + "/";
      if (!system(("mkdir -p " + out_directory).c_str()))
        cv::imwrite( out_directory + image, m_out );

      cudaFree( rgb_d);
      cudaFree( s_d);
      cudaFree( kernel_d);

      cudaFreeHost( g );
      cudaFreeHost( rgb );
    }
  }
  return 0;
}
