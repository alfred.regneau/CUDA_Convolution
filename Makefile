CXX=g++
CXXFLAGS=-O3 -march=native
LDLIBS=`pkg-config --libs opencv`


all: convolution convolution-cu

.PHONY: clean

convolution: convolution.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDLIBS)

convolution-cu: convolution.cu
	nvcc -o $@ $< $(LDLIBS)


clean:
	rm -f convolution
	rm -f convolution-cu
	rm -rf out/
