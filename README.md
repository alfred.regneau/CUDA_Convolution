# Convolution sur des images en C++ et Cuda (pour carte graphique nvidia)

## Voici quelques exemples de convolution sur une image


<div style="text-align: center;">
    <b>Image de départ</b><br>
    <img src="img_readme/cubes480x270.jpg" alt="Image indisponible">
</div>

<div style="text-align: center;">
    <b>Filtre simple box  blur</b><br>
    <img src="img_readme/cubes_box_blur480x270.jpg" alt="Image indisponible">
</div>

<div style="text-align: center;">
    <b>Filtre horizontal line detection</b><br>
    <img src="img_readme/cubes_horizontal480x270.jpg" alt="Image indisponible">
</div>

<div style="text-align: center;">
    <b>Filtre laplacian + gaussian</b><br>
    <img src="img_readme/cubes_laplacian_gaussian480x270.jpg" alt="Image indisponible">
</div>